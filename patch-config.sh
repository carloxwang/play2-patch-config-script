#!/bin/bash
############################################################################
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
# Author: carloxwang                                                       #
# Source: http://git.carloxwang.org/play2-patch-config-script              #
############################################################################
PROJ_HOME="/home/ec2-user/my-play-project"

DEFAULT_PLAY_CONF_FOLDER="conf"

PROD_CONF_FOLDER="conf-prod"
STAGING_CONF_FOLDER="conf-staging"
TEST_CONF_FOLDER="conf-test"

if test -n "$2"; then
    PROJ_HOME="$2"
fi

function printUsage(){
    echo "Invalid Parameter(s)"
    echo "Usage: $0 { staging | prod | test } [HomeFolderPath]"
    exit 0
}


if [ "$#" -ne "1" ] && [ "$#" -ne "2" ]; then
    printUsage;
fi

set -e

case $1 in
    "prod")
        echo "Pathcing for $1 mode!! "
        `cp $PROJ_HOME/$PROD_CONF_FOLDER/* $PROJ_HOME/$DEFAULT_PLAY_CONF_FOLDER/`
        ;;
    "staging")
        echo "Pathcing for $1 mode!! "
        `cp $PROJ_HOME/$STAGING_CONF_FOLDER/* $PROJ_HOME/$DEFAULT_PLAY_CONF_FOLDER/`
        ;;
    "test")
        echo "Pathcing for $1 mode!! "
        `cp $PROJ_HOME/$TEST_CONF_FOLDER/* $PROJ_HOME/$DEFAULT_PLAY_CONF_FOLDER/`
        ;;
    *)
        printUsage
        ;;
esac

echo "Hmmm..... Everything looks fine :). Good Luck"
